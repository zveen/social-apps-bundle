<?php

namespace Zveen\SocialAppsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zveen\SocialAppsBundle\Social\LinkedIn;

class LinkedinController extends Controller
{
    public function indexAction(Request $r)
    {
        /** @var LinkedIn $linkedIn */
        $linkedIn = $this->get('zveen_social_apps.social.linkedin');
        if($linkedIn->canQueryApi() == false){
            $this->get('session')->set('linkedin-accept','ZveenSocialAppsBundle:Linkedin:index');
            $this->get('session')->set('linkedin-cancel','ZveenSocialAppsBundle:Linkedin:cancel');
            return $this->forward('ZveenSocialAppsBundle:Linkedin:auth');
        }
        // query linked in
        $data = "<?xml version='1.0' encoding='UTF-8'?>
<share>
  <comment>I'm recruiting for: ASDFASDASD</comment>
  <content>
    <title>Click here to apply for job</title>
    <description>ASDASDASD</description>
    <submitted-url>http://www.visionforeducation.co.uk/asdfasdf</submitted-url>
  </content>
  <visibility>
    <code>anyone</code>
  </visibility>
</share>";
        $response = $linkedIn->postApi('https://api.linkedin.com/v1/people/~/shares', $data);

        return new Response('DONE <a href="https://api.linkedin.com/v1/people/~?oauth2_access_token='.$linkedIn->getAccessToken().'">Test</a>' . $response);
    }

    public function cancelAction(Request $r)
    {
        return new Response('CANCELED');
    }

    public function authAction(Request $r)
    {
        /** @var LinkedIn $linkedIn */
        $linkedIn = $this->get('zveen_social_apps.social.linkedin');
        if($linkedIn->canQueryApi() == false){
            try{
                $redirect = $linkedIn->handleLogin($r);
                if($redirect !== null)
                    return $redirect;
            }catch (\Exception $e){
                $cancelAction = $this->get('session')->get('linkedin-cancel');
                $this->get('session')->remove('linkedin-accept');
                $this->get('session')->remove('linkedin-cancel');
                var_dump($e->getMessage());
                exit();
                return $this->forward($cancelAction);
            }

        }

        // forward to required action
        $forwardAction = $this->get('session')->get('linkedin-accept');
        $this->get('session')->remove('linkedin-accept');
        $this->get('session')->remove('linkedin-cancel');
        return $this->forward($forwardAction);
    }
}
