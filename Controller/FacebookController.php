<?php

namespace Zveen\SocialAppsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zveen\SocialAppsBundle\Social\Facebook;

class FacebookController extends Controller
{
    public function indexAction(Request $r)
    {
        /** @var Facebook $facebook */
        $facebook = $this->get('zveen_social_apps.social.facebook');
        if($facebook->canQueryApi() == false){
            $this->get('session')->set('facebook-accept','ZveenSocialAppsBundle:Facebook:index');
            $this->get('session')->set('facebook-cancel','ZveenSocialAppsBundle:Facebook:cancel');
            return $this->forward('ZveenSocialAppsBundle:Facebook:auth');
        }

        try {
            $ret_obj = $facebook->sdk->api('/me/feed', 'POST',
                array(
                    'link' => 'www.example.com',
                    'message' => 'Posting with the PHP SDK!'
                ));
            echo '<pre>Post ID: ' . $ret_obj['id'] . '</pre>';

            echo '<br /><a href="' . $facebook->getLogoutUrl() . '">logout</a>';
        } catch(FacebookApiException $e) {
            $login_url = $facebook->sdk->getLoginUrl( array(
                'scope' => 'publish_stream'
            ));
            echo 'Please <a href="' . $login_url . '">login.</a>';
            var_dump($e->getType());
            var_dump($e->getMessage());
        }

        return new Response('DONE <a href="https://api.linkedin.com/v1/people/~?oauth2_access_token='.'">Test</a>' . $facebook->getApi('https://api.facebook.com/1.1/statuses/user_timeline.json'));
    }

    public function cancelAction(Request $r)
    {
        return new Response('CANCELED');
    }

    public function authAction(Request $r)
    {
        /** @var Facebook $facebook */
        $facebook = $this->get('zveen_social_apps.social.facebook');
        if($facebook->canQueryApi() === false){
            try{
                $redirect = $facebook->handleLogin($r,array('manage_pages'));
                if($redirect !== null)
                    return $redirect;
            }catch (\Exception $e){
                $cancelAction = $this->get('session')->get('facebook-cancel');
                $this->get('session')->remove('facebook-accept');
                $this->get('session')->remove('facebook-cancel');
                return $this->forward($cancelAction);
            }

        }

        // forward to required action
        $forwardAction = $this->get('session')->get('facebook-accept');
        $this->get('session')->remove('facebook-accept');
        $this->get('session')->remove('facebook-cancel');
        return $this->forward($forwardAction);
    }
}
