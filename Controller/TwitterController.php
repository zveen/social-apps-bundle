<?php

namespace Zveen\SocialAppsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Zveen\SocialAppsBundle\Social\Twitter;

class TwitterController extends Controller
{
    public function indexAction(Request $r)
    {
        /** @var Twitter $twitter */
        $twitter = $this->get('zveen_social_apps.social.twitter');
        if($twitter->canQueryApi() == false){
            $this->get('session')->set('twitter-accept','ZveenSocialAppsBundle:Twitter:index');
            $this->get('session')->set('twitter-cancel','ZveenSocialAppsBundle:Twitter:cancel');
            return $this->forward('ZveenSocialAppsBundle:Twitter:auth');
        }

        try{
            $resp = $twitter->getApi('https://api.twitter.com/1.1/statuses/user_timeline.json');
        }catch (\Exception $e){

        }

        return new Response('DONE <a href="https://api.linkedin.com/v1/people/~?oauth2_access_token='.'">Test</a>' . $resp);
    }

    public function cancelAction(Request $r)
    {
        return new Response('CANCELED');
    }

    public function authAction(Request $r)
    {
        /** @var Twitter $twitter */
        $twitter = $this->get('zveen_social_apps.social.twitter');
        if($twitter->canQueryApi() == false){
            try{
                $redirect = $twitter->handleLogin($r);
                if($redirect !== null)
                    return $redirect;
            }catch (\Exception $e){
                $cancelAction = $this->get('session')->get('twitter-cancel');
                $this->get('session')->remove('twitter-accept');
                $this->get('session')->remove('twitter-cancel');
                var_dump($e->getMessage());
                exit();
                return $this->forward($cancelAction);
            }

        }

        // forward to required action
        $forwardAction = $this->get('session')->get('twitter-accept');
        $this->get('session')->remove('twitter-accept');
        $this->get('session')->remove('twitter-cancel');
        return $this->forward($forwardAction);
    }
}
