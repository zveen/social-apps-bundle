<?php

namespace Zveen\SocialAppsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('zveen_social_apps');

        $rootNode
            ->children()
                ->booleanNode('debug')->cannotBeEmpty()->end()
                ->arrayNode('linkedin')->canBeEnabled()
                    ->children()
                        ->scalarNode('apiKey')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('secretKey')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('oauthUserToken')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('oauthUserSecret')->isRequired()->cannotBeEmpty()->end()
                        ->variableNode('scope')->end()
                        ->scalarNode('redirectUri')->isRequired()->cannotBeEmpty()->end()
                    ->end()
                ->end()
                ->arrayNode('twitter')->canBeEnabled()
                    ->children()
                        ->scalarNode('consumerKey')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('consumerSecret')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('accessToken')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('accessTokenSecret')->isRequired()->cannotBeEmpty()->end()
                        ->scalarNode('redirectUri')->isRequired()->cannotBeEmpty()->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
