<?php

namespace Zveen\SocialAppsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class ZveenSocialAppsExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);
        $config['linkedin']['debug'] = $config['debug'];
        $config['twitter']['debug'] = $config['debug'];
        $config['facebook']['debug'] = $config['debug'];
        $container->setParameter('zveen_social_apps.linkedin',$config['linkedin']);
        $container->setParameter('zveen_social_apps.twitter',$config['twitter']);
        $container->setParameter('zveen_social_apps.facebook',$config['facebook']);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
