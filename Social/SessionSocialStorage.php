<?php

namespace Zveen\SocialAppsBundle\Social;


use Symfony\Component\HttpFoundation\Session\Session;

class SessionSocialStorage implements ISocialStorage {

    private $_session;

    public function __construct(Session $session){
        $this->_session = $session;
    }

    public function getLinkedInAccessToken()
    {
        return $this->_session->has('zveen.social.linkedin.accesstoken')?
            $this->_session->get('zveen.social.linkedin.accesstoken'):
            null;
    }

    public function setLinkedInAccessToken($accessToken, \DateTime $expires)
    {
        $this->_session->set('zveen.social.linkedin.accesstoken',$accessToken);
    }

    /**
     * @return null
     */
    public function cleanLinkedInAccessToken()
    {
        $this->_session->remove('zveen.social.linkedin.accesstoken');
    }

    /**
     * @return string|null
     */
    public function getLinkedInState()
    {
        return $this->_session->has('zveen.social.linkedin.state')?
            $this->_session->get('zveen.social.linkedin.state'):
            null;
    }

    /**
     * @param $state string
     * @return null
     */
    public function setLinkedInState($state)
    {
        $this->_session->set('zveen.social.linkedin.state',$state);
    }

    /**
     * @return null
     */
    public function cleanLinkedInState()
    {
        $this->_session->remove('zveen.social.linkedin.state');
    }


    /**
     * @return string
     */
    public function getTwitterRequestToken()
    {
        return $this->_session->get('zveen.social.twitter.request_token');
    }

    /**
     * @param $requestToken string
     * @return null
     */
    public function setTwitterRequestToken($requestToken)
    {
        $this->_session->set('zveen.social.twitter.request_token', $requestToken);
    }

    /**
     * @return string
     */
    public function getTwitterRequestTokenSecret()
    {
        return $this->_session->get('zveen.social.twitter.request_token_secret');
    }

    /**
     * @param $requestTokenSecret string
     * @return null
     */
    public function setTwitterRequestTokenSecret($requestTokenSecret)
    {
        $this->_session->set('zveen.social.twitter.request_token_secret', $requestTokenSecret);
    }

    /**
     * @return string
     */
    public function getTwitterToken()
    {
        return $this->_session->get('zveen.social.twitter.token');
    }

    /**
     * @param $token string
     * @return null
     */
    public function setTwitterToken($token)
    {
        $this->_session->set('zveen.social.twitter.token', $token);
    }

    /**
     * @return string
     */
    public function getTwitterTokenSecret()
    {
        return $this->_session->get('zveen.social.twitter.token_secret');
    }

    /**
     * @param $tokenSecret string
     * @return null
     */
    public function setTwitterTokenSecret($tokenSecret)
    {

        $this->_session->set('zveen.social.twitter.token_secret', $tokenSecret);
    }

    /**
     * @return null
     */
    public function cleanTwitterData()
    {
        $this->_session->remove('zveen.social.twitter.token');
        $this->_session->remove('zveen.social.twitter.token_secret');
        $this->_session->remove('zveen.social.twitter.request_token');
        $this->_session->remove('zveen.social.twitter.request_token_secret');
    }


}