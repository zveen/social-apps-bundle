<?php

namespace Zveen\SocialAppsBundle\Social;


use Monolog\Logger;
use Symfony\Component\HttpFoundation\Request;

abstract class SocialBase {
    /** @var  Logger */
    protected  $_logger;

    /** @var  ISocialStorage */
    protected  $_storage;

    /** @var  bool */
    public $debug;

    public function __construct(ISocialStorage $storage = null,$logger = null){
        $this->_storage = $storage;
        $this->_logger = $logger;
    }

    /**
     * @return bool
     */
    public abstract function canQueryApi();
    public abstract function handleLogin(Request $request);
} 