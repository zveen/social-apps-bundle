<?php

namespace Zveen\SocialAppsBundle\Social;


use BaseFacebook;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class Facebook extends SocialBase{

    /** @var BaseFacebook */
    public $sdk;

    private $_kernel;


    public function __construct(array $options, BaseFacebook $fb, $kernel, $logger = null){
        parent::__construct(null,$logger);
        $this->sdk = $fb;
        $this->_kernel = $kernel;
        $this->debug = $options['debug'];
    }

    /**
     * @return bool
     */
    public function canQueryApi()
    {
        $user = $this->sdk->getUser();
        if($user === 0){
            $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->canQueryApi = false');
            return false;
        }
        try {
            $this->api('/me/feed');


        } catch(\FacebookApiException $e) {
            $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->canQueryApi = false');
            return false;
        }
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->canQueryApi = true');
        return true;
    }

    public function handleLogin(Request $request, array $options = array())
    {
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->handleLogin');

        return new RedirectResponse($this->sdk->getLoginUrl($options));
    }

    public function cleanData(){
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->cleanData');
        $this->sdk->destroySession();
    }

    public function api(){
        $args = func_get_args();
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->api ' . $args[0]);
        return call_user_func_array(array($this->sdk, 'api'), $args);
    }

    public function hasPermissions($permissions){
        // Check for permissions
        $perms = $this->sdk->api("/me/permissions");
        foreach($permissions as $p){
            if(array_key_exists($p, $perms['data'][0]) === false){
                $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->hasPermissions = false');
                return false;
            }
        }
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Facebook->hasPermissions = true');
        return true;
    }
}