<?php

namespace Zveen\SocialAppsBundle\Social;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class Twitter extends SocialBase{

    /** @var string */
    private $_consumerKey;
    /** @var string */
    private $_consumerSecret;
    /** @var string */
    private $_accessToken;
    /** @var string */
    private $_accessTokenSecret;
    /** @var string */
    private $_redirectUri;

    private $_kernel;

    public function __construct(array $options, ISocialStorage $storage, $kernel, $logger = null){
        parent::__construct($storage,$logger);
        $this->_consumerKey = $options['consumerKey'];
        $this->_consumerSecret = $options['consumerSecret'];
        $this->_accessToken = $options['accessToken'];
        $this->_accessTokenSecret = $options['accessTokenSecret'];
        $this->_redirectUri = $options['redirectUri'];
        $this->debug = $options['debug'];
        $this->_kernel = $kernel;
    }

    /**
     * @return bool
     */
    public function canQueryApi()
    {
        $result = $this->_storage->getTwitterToken() && $this->_storage->getTwitterTokenSecret();
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->canQueryApi = ' . ($result?'true':'false') );
        return $result;
    }

    public function cleanData(){
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->cleanData');
        $this->_storage->cleanTwitterData();
    }

    public function getTwitterToken(){
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->getTwitterToken = ' . $this->_storage->getTwitterToken());
        return $this->_storage->getTwitterToken();;
    }

    public function getTwitterTokenSecret(){
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->getTwitterToken = ' . $this->_storage->getTwitterTokenSecret());
        return $this->_storage->getTwitterTokenSecret();
    }

    public function handleLogin(Request $request, array $scope = array())
    {
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->handleLogin START');
        // If we have returned from redirect
        if( ($request->query->has('oauth_token') && $request->query->has('oauth_verifier')) || $request->query->has('denied')){
            $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Returned from redirect');
            if($request->query->has('denied')){
                $this->debug && $this->_logger !== null && $this->_logger->addError('   User has denied authentication');
                throw new \Exception('User has denied authentication');
            }

            if($request->query->get('oauth_token') !== $this->_storage->getTwitterRequestToken()){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    Returned token doesn\'t match stored TwitterRequestToken');
                throw new \Exception('Returned token doesn\'t match stored TwitterRequestToken.');
            }

            $authorizationHeaderData = array(
                'oauth_token' => $this->_storage->getTwitterRequestToken(),
                'oauth_consumer_key' => $this->_consumerKey,
            );
            $bodyData = 'oauth_verifier=' . $request->query->get('oauth_verifier');
            parse_str($bodyData,$bodyDataArray);
            $this->addOAuthData('POST', 'https://api.twitter.com/oauth/request_token', $authorizationHeaderData, $bodyDataArray);

            $authorizationHeaderDataStrings = array();
            foreach($authorizationHeaderData as $key => $value){
                $authorizationHeaderDataStrings[] = $key . '="' . rawurlencode($value) . '"';
            }
            $headers[] = 'Authorization: OAuth ' . join(', ', $authorizationHeaderDataStrings);
            $headers[] = 'Content-Length: '.strlen($bodyData);
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';

            $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Requesting access token');
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/oauth/access_token');
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyData);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CAINFO, $this->_kernel->locateResource('@ZveenSocialAppsBundle/Resources/cacert.pem'));
            $response = curl_exec($ch);
            $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

            if($httpStatus != 200){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    ' . $response);
                throw new \Exception($response);
            }
            if(!$response){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    ' . curl_error($ch));
                throw new \Exception(curl_error($ch));
            }

            parse_str($response, $data);

            $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Setting TwitterToken and TwitterTokenSecret');
            $this->_storage->setTwitterToken($data['oauth_token']);
            $this->_storage->setTwitterTokenSecret($data['oauth_token_secret']);

            $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->handleLogin END');
            return;
        }

        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    No redirect detected');
        // Authorization
        $authorizationHeaderData = array(
            'oauth_callback' => $this->_redirectUri,
            'oauth_consumer_key' => $this->_consumerKey,
        );
        $bodyData = '';
        $this->addOAuthData('POST', 'https://api.twitter.com/oauth/request_token', $authorizationHeaderData);

        $authorizationHeaderDataStrings = array();
        foreach($authorizationHeaderData as $key => $value){
            $authorizationHeaderDataStrings[] = $key . '="' . rawurlencode($value) . '"';
        }
        $headers[] = 'Authorization: OAuth ' . join(', ', $authorizationHeaderDataStrings);
        $headers[] = 'Content-Length: '.strlen($bodyData);

        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Fetching TwitterRequestToken');
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://api.twitter.com/oauth/request_token');
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CAINFO, $this->_kernel->locateResource('@ZveenSocialAppsBundle/Resources/cacert.pem'));
        $response = curl_exec($ch);
        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($httpStatus != 200){
            $this->debug && $this->_logger !== null && $this->_logger->addError('    ' . $response);
            throw new \Exception($response);
        }
        parse_str($response, $data);
        if($data['oauth_callback_confirmed'] !== 'true'){
            $this->debug && $this->_logger !== null && $this->_logger->addError('    OAuth callback not confirmed');
            throw new \Exception('OAuth callback not confirmed');
        }

        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Storing TwitterRequestToken and TwitterRequestTokenSecret');
        $this->_storage->setTwitterRequestToken($data['oauth_token']);
        $this->_storage->setTwitterRequestTokenSecret($data['oauth_token_secret']);

        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Redirecting to auth');
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->handleLogin END');
        return new RedirectResponse(
            'https://api.twitter.com/oauth/authenticate?oauth_token=' .
            $data['oauth_token']);
    }

    public function api($url, $method = 'GET', array $data = array())
    {
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->api START');
        // Authorization
        $authorizationHeaderData = array(
            'oauth_consumer_key' => $this->_consumerKey,
            'oauth_token' => $this->_storage->getTwitterToken()
        );
        $this->addOAuthData($method, $url, $authorizationHeaderData, $data);
        $authorizationHeaderDataStrings = array();
        foreach($authorizationHeaderData as $key => $value){
            $authorizationHeaderDataStrings[] = $key . '="' . rawurlencode($value) . '"';
        }

        $bodyDataStrings = array();
        foreach($data as $key => $value){
            $bodyDataStrings[] = $key . '=' . rawurlencode($value) . '';
        }
        $bodyData = join('&',$bodyDataStrings);
        $headers[] = 'Authorization: OAuth ' . join(', ', $authorizationHeaderDataStrings);
        $headers[] = 'Content-Length: ' . strlen($bodyData);
        if($method === 'POST')
            $headers[] = 'Content-Type: application/x-www-form-urlencoded';

        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    ' . $method . ' ' . $url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($ch, CURLOPT_POST, $method === 'POST');
        if($method === 'POST')
            curl_setopt($ch, CURLOPT_POSTFIELDS, $bodyData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CAINFO, $this->_kernel->locateResource('@ZveenSocialAppsBundle/Resources/cacert.pem'));
        $response = curl_exec($ch);
        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if($httpStatus != 200){
            // If we got the non 200 check for duplicate error and supress it
            // Throw all the other errors after logging
            $json = json_decode($response);
            $hasError = false;
            foreach($json->errors as $error){
                switch($error->message){
                    case 'Status is a duplicate.': {
                        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Status is a duplicate. Continuing normaly');
                    } break;
                    default: {
                        $this->debug && $this->_logger !== null && $this->_logger->addError('    ' . $error->message);
                        $hasError = true;
                    }
                }
            }
            if($hasError){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    ' . print_r(curl_getinfo($ch),true));
                throw new \Exception($response);
            }
        }

        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->api END');
        return $response;
    }

    private function addOAuthData($method, $url, array &$data, array $queryParams = array()){
        $data['oauth_nonce'] =  md5(mt_rand());
        $data['oauth_signature_method'] = 'HMAC-SHA1';
        $data['oauth_timestamp'] = time();
        $data['oauth_version'] = '1.0';

        // Percent encode all the keys and values
        $encodedData = array();
        foreach(array_merge($data,$queryParams) as $key => $value){
            $encodedData[rawurlencode($key)] = rawurlencode($value);
        }
        ksort($encodedData);
        $parts = array();
        foreach($encodedData as $key => $value){
            $parts[] = $key . '=' . $value;
        }
        $partsString = join('&',$parts);
        $sigBase = strtoupper($method) . '&' . rawurlencode($url) . '&' . rawurlencode($partsString);
        $sigKey = rawurlencode($this->_consumerSecret) . '&' . rawurlencode($this->_storage->getTwitterTokenSecret() !== null?$this->_storage->getTwitterTokenSecret():'');

        $data['oauth_signature'] =  base64_encode(hash_hmac('sha1', $sigBase, $sigKey, true));
    }
}