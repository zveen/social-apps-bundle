<?php

namespace Zveen\SocialAppsBundle\Social;


use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

class LinkedIn extends SocialBase{

    /** @var string */
    private $_apiKey;
    /** @var string */
    private $_secretKey;
    /** @var string */
    private $_oauthUserToken;
    /** @var string */
    private $_oauthUserSecret;
    /** @var array */
    private $_scope;
    /** @var string */
    private $_redirectUri;

    private $_kernel;

    public function __construct(array $options, ISocialStorage $storage, $kernel, $logger = null){
        parent::__construct($storage,$logger);
        $this->debug = $options['debug'];
        $this->_apiKey = $options['apiKey'];
        $this->_secretKey = $options['secretKey'];
        $this->_oauthUserToken = $options['oauthUserToken'];
        $this->_oauthUserSecret = $options['oauthUserSecret'];
        $this->_scope = $options['scope'];
        $this->_redirectUri = $options['redirectUri'];
        $this->_kernel = $kernel;
    }

    /**
     * @return bool
     */
    public function canQueryApi()
    {
        $result = $this->_storage->getLinkedInAccessToken() !== null;
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('LinkedIn->canQueryApi = ' . ($result?'true':'false') );
        return $result;
    }

    public function cleanData(){
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('LinkedIn->cleanData');
        $this->_storage->cleanLinkedInAccessToken();

    }
    public function getAccessToken(){
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('LinkedIn->getAccessToken = ' . $this->_storage->getLinkedInAccessToken() );
        return $this->_storage->getLinkedInAccessToken();
    }

    public function handleLogin(Request $request, array $scope = array())
    {
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('Twitter->handleLogin START');
        // If we have returned from redirect
        if($request->query->has('code') || $request->query->has('error')){
            $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Returned from redirect');

            if($request->query->has('error')){
                $this->debug && $this->_logger !== null && $this->_logger->addError('   Error returned: ' . $request->query->get('error_description'));
                throw new \Exception($request->query->get('error_description'));
            }
            if($request->query->get('state') !== $this->_storage->getLinkedInState()){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    Returned state doesn\'t match stored state');
                throw new \Exception('State mismatch. Possible CSRF attack');
            }
            $this->_storage->cleanLinkedInState();

            if(!$request->query->has('code')){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    No authorization code returned');
                throw new \Exception('No authorization code returned');
            }
            $authCode = $request->query->get('code');
            $accessTokenUrl = 'https://www.linkedin.com/uas/oauth2/accessToken';
            $accessTokenParams = array(
                'grant_type' => 'authorization_code',
                'code' => $authCode,
                'redirect_uri' => $this->_redirectUri,
                'client_id' => $this->_apiKey,
                'client_secret' => $this->_secretKey
            );

            $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Requesting access token');

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $accessTokenUrl);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($accessTokenParams));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_CAINFO, $this->_kernel->locateResource('@ZveenSocialAppsBundle/Resources/cacert.pem'));
            $response = curl_exec($ch);

            if(!$response){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    ' . curl_error($ch));
                throw new \Exception(curl_error($ch));
            }

            curl_close ($ch);
            $data = json_decode($response);

            if(property_exists($data, 'error')){
                $this->debug && $this->_logger !== null && $this->_logger->addError('    Api returned error: ' . $data->error_description);
                throw new \Exception($data->error_description);
            }

            $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Saving access token');
            $expires = new \DateTime();
            $expires = $expires->add(new \DateInterval('PT'.$data->expires_in.'S'));
            $this->_storage->setLinkedInAccessToken($data->access_token,$expires);
            return;
        }

        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    No redirect detected');
        // Authorization
        $scope = array_unique(array_merge($this->_scope, $scope));
        $state = md5(mt_rand());
        $this->_storage->setLinkedInState($state);


        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    Redirecting to auth');
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('LinkedIn->handleLogin END');
        return new RedirectResponse(
            'https://www.linkedin.com/uas/oauth2/authorization?response_type=code'
                . '&client_id=' . $this->_apiKey
                . '&scope=' . rawurlencode(join(' ', $scope))
                . '&state=' . rawurlencode($state)
                . '&redirect_uri=' . rawurlencode($this->_redirectUri));

    }

    public function api($url, $method, $data){
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('LinkedIn->api START');
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('    ' . $method . ' ' . $url);

        $url .= "?oauth2_access_token=" . $this->_storage->getLinkedInAccessToken();
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json','x-li-format: json'));
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        if($method === 'POST'){
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CAINFO, $this->_kernel->locateResource('@ZveenSocialAppsBundle/Resources/cacert.pem'));
        $response = json_decode(curl_exec($ch));
        $httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if($httpStatus >= 400){
            $this->debug && $this->_logger !== null && $this->_logger->addError('    Error: ' . curl_error($ch));
            $this->debug && $this->_logger !== null && $this->_logger->addError('    Response: ' . $response);
            $exc = new \Exception($response->message);
            curl_close($ch);
            throw $exc;
        }
        $this->debug && $this->_logger !== null && $this->_logger->addInfo('LinkedIn->api END');
        return $response;
    }
}