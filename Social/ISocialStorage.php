<?php

namespace Zveen\SocialAppsBundle\Social;

interface ISocialStorage {
    /**
     * @return string|null
     */
    public function getLinkedInAccessToken();

    /**
     * @param $accessToken string
     * @param $expires \Datetime
     * @return null
     */
    public function setLinkedInAccessToken($accessToken,\DateTime $expires);

    /**
     * @return null
     */
    public function cleanLinkedInAccessToken();

    /**
     * @return string|null
     */
    public function getLinkedInState();

    /**
     * @param $state string
     * @return null
     */
    public function setLinkedInState($state);

    /**
     * @return null
     */
    public function cleanLinkedInState();

    /**
     * @return string
     */
    public function getTwitterRequestToken();

    /**
     * @param $oauthToken string
     * @return null
     */
    public function setTwitterRequestToken($oauthToken);

    /**
     * @return string
     */
    public function getTwitterRequestTokenSecret();

    /**
     * @param $oauthTokenSecret string
     * @return null
     */
    public function setTwitterRequestTokenSecret($oauthTokenSecret);

    /**
     * @return string
     */
    public function getTwitterToken();

    /**
     * @param $oauthToken string
     * @return null
     */
    public function setTwitterToken($oauthToken);

    /**
     * @return string
     */
    public function getTwitterTokenSecret();

    /**
     * @param $oauthTokenSecret string
     * @return null
     */
    public function setTwitterTokenSecret($oauthTokenSecret);

    /**
     * @return null
     */
    public function cleanTwitterData();

} 